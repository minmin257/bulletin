require('./bootstrap');
import "@fortawesome/fontawesome-free/css/all.min.css";
// Import modules...
import { createApp, h } from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import LitepieDatepicker from 'litepie-datepicker';

const el = document.getElementById('app');

let app = createApp({
    render: () =>
        h(InertiaApp, {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: (name) => require(`./Pages/${name}`).default,
        }),
})
    .mixin({ methods: { route } })
    .use(InertiaPlugin)
    .use(VueSweetalert2)
    .use(LitepieDatepicker)
    // .component('basic-tree', BasicTree)
    .mount(el);


InertiaProgress.init({ color: '#4B5563' });
