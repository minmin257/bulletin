<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> --}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>佈告欄</title>
    <!-- <link rel="shortcut icon" href="images/shortcut_icon.png" type="image/x-icon"> -->
    <script src="https://kit.fontawesome.com/d17564c281.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://vjs.zencdn.net/5-unsafe/video-js.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="/frontend/path/to/nice-select.css">
    <link rel="stylesheet" href="/frontend/css/semantic.min.css">
    <link rel='stylesheet' href='/frontend/css/main.css'>
    <link rel="stylesheet" href="/frontend/css/vue-multiselect.min.css">
    
    <style>
        .main-blog__time {
            position: relative;
            right: auto;
            bottom: auto;
            margin-right: 20px;
        }

        .flatpickr-day.selected {
            background-color: #353340;
            border-color: #353340;
        }

        .flatpickr-2 {
            opacity: 1;
        }

        @media screen and (max-width: 767px) {
            .main-blog__time {
                top: auto;
                width: fit-content;
                margin-bottom: 10px;
            }
        }

        @media screen and (max-width: 476px) {
            .main-blog__time {
                margin-left: 10px;
            }
        }

        .flatpickr-input-body {
            border-radius: 50px;
        }

        .flatpickr-input-body .flatpickr-input {
            width: 230px;
            border: none;
            background-color: #f1f4fa;
            color: #353340 !important;
            font-family: "Gen-M", "Inter", sans-serif;
            font-size: 14px;
            border-radius: 50px;
            opacity: 1;
            padding-left: 42px;
            padding-right: 15px
        }

        .flatpickr-icon {
            left: 25px;
        }

        .main-blog__title {
            font-size: 15px;
        }

        @media screen and (max-width: 575px) {
            .header {
                display: block;
            }

            .flatpickr-input-body {
                margin-left: 0
            }

            .search-bar {
                margin-bottom: 15px;
            }
        }

        .members-card {
            height: 100px;
        }

        .members-card-name {
            margin-bottom: 0;
        }

        .main-blog__time {
            margin-bottom: 8px;
            margin-right: 0;
        }

        .main-blog__types {
            background-color: #adadad;
            color: #fff;
            padding: 5px 10px;
            border-radius: 6px;
            width: max-content;
            font-size: 14px;
        }

        .main-blog__title {
            margin-left: 10px;
        }

        .newsContent-area .main-blog__types {
            position: absolute;
            right: 135px;
            bottom: 20px;
        }

    </style>

</head>

<body>
    @yield('content')
    <!-- footer================================================== -->
    @include('frontend/layout/footer')
    <!-- rightBTN================================================== -->
    <!-- <div class="rightBTN"></div> -->
    <div class="md-overlay"></div>
    <!-- JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="/frontend/js/flatpickr.min.js"></script>
    <script src="/frontend/js/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="/frontend/js/semantic.min.js"></script>
    <script src="/frontend/js/rxjs.umd.js"></script>
    <script src="/frontend/js/vue-multiselect.min.js"></script>
    <script src="/frontend/js/lodash.min.js"></script>


    <script type="text/javascript" src='/frontend/js/main.js'></script>

</body>

</html>


<script>

    $(document).ready(function() {

        $(".flatpickr")
            .flatpickr({
                mode: "range",
                dateFormat: "Y-m-d"
            });
        $(".flatpickr-2")
            .flatpickr()
    });

</script>



{{-- @yield('js'); --}}
