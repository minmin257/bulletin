@extends('frontend.index')


@section('content')

    <!-- header================================================== -->
    @include('frontend.components.test')
    @include('frontend.components.news_modal')

    <div class="app" id="app">
        @include('frontend.components.sidebar')
        <div class="wrapper">
            @include('frontend.components.navbar')
            <div class="main-container">
                <div class="main-body main-blogs bulletin" id="news_list">
                    <div class="main-header anim" style="animation-delay: 0s;">News
                        <div class="button-group">
                            @if ($newsPermission)
                                <button class="add md-trigger" v-on:click="openNewsModal()" data-modal="modal-5"
                                    type="button"><img
                                        src="https://img.icons8.com/ios-filled/18/ffffff/plus-math.png" /></button>
                                <button class="onoff" id="onoff" v-on:click="updateEnable()" type="button"><img
                                        src="https://img.icons8.com/metro/18/ffffff/switch-off.png" /></button>
                                <button class="del" id="del" type="button" v-on:click="deleteNews()"><img
                                        src="https://img.icons8.com/fluent-systems-regular/18/ffffff/trash--v2.png" /></button>
                            @endif

                        </div>
                    </div>
                    <!--news-->
                    {{-- <div id="vue_test"></div> --}}
                    <div id="news_hole">
                        <input type="hidden" id="allNewsCount" value="103">

                        <div v-for="(item) in news?.data" class="main-blog anim" id="news_block_46" v-bind:class="{ 'off': !item.enable }">
                            @if ($newsPermission)
                                <input type="checkbox" v-model="checkedBox" name="select_news" :value="item.id">
                            @endif


                            <div class="main-blog__author" style="min-width:auto">
                                <div class="author-img__wrapper">
                                </div>
                            </div>
                            <div>
                                
                                <div class="main-blog__time " v-bind:class="{ 'bg-gray': !item.enable }"
                                    style="font-size: 14px">@{{ item . date }}</div>
                                <div class="main-blog__types" v-if="item.news_type.length > 0">
                                    <span v-for="(type,index) in item.news_type">
                                        <span v-if="index != 0">|</span> @{{ type . display_name }}

                                    </span>

                                </div>
                            </div>
                            <div class="main-blog__title">
                                @if ($newsPermission)
                                    <a :href="'/edit-news/'+item.id">
                                        @{{ item . content }}
                                    </a>

                                @else
                                    <span> @{{ item . content }}</span>
                                @endif

                            </div>
                        </div>


                    </div>
                </div>
                <!--news content&model-->
                <div id="news_modal">
                </div>

            </div>
        </div>
    </div>

    <script type="module">
        import Vue from '/frontend/js/vue.esm.browser.js'

        new Vue({
            el: "#news_list",
            data: {
                searchInput: {
                    date: [],
                    keyWord: ''
                },
                news: {},
                checkedBox: [],
            },
            created: function() {
                this.getNews();
                searchNewSubject.subscribe((v) => {
                    this.searchInput = v;
                    this.getNews();
                });


            },
            methods: {
                getNews(page = 1) {
                    let searchStr = '';
                    searchStr += this.searchInput.keyWord ? `&keyword=${this.searchInput.keyWord}` : ''
                    searchStr += this.searchInput.date.length > 0 ?
                        `&startDay=${this.searchInput.date[0]}&endDay=${this.searchInput.date[1]}` : ''
                    axios.get(`/api/news?page=${page}${searchStr}`).then(x => {
                        this.news = x.data;
                    })
                },
                openNewsModal() {
                    let setting = {
                        target: 'news',
                        action: 'open'
                    };
                    sendSetModal(setting);
                },
                updateEnable() {
                    if (this.checkedBox.length == 0) return;
                    let ids = this.checkedBox;
                    axios.put('/api/news/change-enable', {}, {
                        params: {
                            ids: ids
                        }
                    }).then(x => {
                        newOldSearch();
                        Swal.fire({
                            title: '修改成功',
                            icon: 'success',
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 1800,
                            width: '300px',
                            heightAuto: false
                        });
                        this.checkedBox = []
                    })

                },
                deleteNews() {
                    if (this.checkedBox.length == 0) return;
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger',
                            title: 'delete-swal-title',
                        },
                        buttonsStyling: false,
                        width: '300px'
                    })

                    swalWithBootstrapButtons.fire({
                        title: '您確定要刪除嗎？',
                        html: '<span style="color: #707070;font-size: 14px;">此動作將不可回復<span>',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '確定',
                        cancelButtonText: '取消',
                        reverseButtons: true,
                    }).then((result) => {
                        if (result.value) {
                            let ids = this.checkedBox;

                            axios.delete('/api/news/delete-many', {
                                params: {
                                    ids: ids
                                }
                            }).then(x => {
                                newOldSearch();
                                Swal.fire({
                                    title: '刪除成功',
                                    icon: 'success',
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    timer: 1800,
                                    width: '300px',
                                    heightAuto: false
                                });
                                this.checkedBox = []
                            })
                        }
                    })

                }
            },
        });
    </script>


    <style>
        .bg-gray {
            background-color: #8e8e8e;
        }

    </style>

@endsection
