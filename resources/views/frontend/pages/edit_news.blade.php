@extends('frontend.index')


@section('content')

    {{-- @include('frontend.components.test') --}}
    <div class="app" id="app">
        @include('frontend.components.sidebar')
        <div class="wrapper">
            @include('frontend.components.navbar')
            <div class="main-container" id="news_page">

                {{-- <div id="news_modal">
                </div> --}}

                <div class="main-body newsContent-area content">
                    <div class="main-header newsContent-title anim" style="padding-left: 20px">

                        @if ($canEdit)
                            <div class="button-group ">
                                <button class="edit" v-on:click="openModal({{ $news->id }})"><img
                                        src="https://img.icons8.com/fluent-systems-regular/18/ffffff/edit--v1.png" /></button>
                            </div>
                        @endif


                        <div class="newsContent-title-text" style="max-width:80% !important">
                            {{ $news->content }}

                            <div class="newsContent-title-time" style="font-size: 14px !important">{{ $news->date }}</div>
                            @if (count($news->news_type) != 0)
                                <div class="main-blog__types">

                                    @foreach ($news->news_type as $news_type)

                                        @if ($loop->index != 0)
                                            |
                                        @endif
                                        {{ $news_type->display_name }}
                                    @endforeach

                                </div>
                            @endif

                        </div>

                    </div>

                </div>




                {{-- modal --}}

                <!--新增消息-->
                <div class="md-modal md-effect-1" v-bind:class="{ 'md-show': show }">
                    <div class="md-content">
                        <h5>編輯消息
                            <button class="md-close" v-on:click="show = false"><img
                                    src="https://img.icons8.com/ios-filled/14/353340/delete-sign--v2.png" /></button>
                        </h5>
                        <div class="md-content-body">
                            <div class="member-input">
                                <div class="member">
                                    <label for="">內容 Content</label>
                                    <textarea v-model="form.content" name="" id="" cols="30" rows="10"></textarea>
                                </div>
                                <div class="member">
                                    <label for="">消息分類 Sort</label>
                                    <multiselect v-model="selected" placeholder="分類" label="display_name" track-by="id"
                                        :options="newsType" :multiple="true" :taggable="true"></multiselect>
                                </div>
                                <div class="member">
                                    <label for="">日期 Date</label>
                                    <input class="flatpickr flatpickr-2 flatpickr-input active" type="date"
                                        v-model="form.date" placeholder="選擇日期" readonly="readonly"
                                        style="opacity: 1; height: auto;">
                                </div>
                                <div class="member input-group spinner">
                                    <label for="">排序 Sort</label>
                                    <div style="display: flex;align-items: center">
                                        <div class="input-group-prepend">
                                            <button class="btn text-monospace " v-on:click="lessSort()"
                                                type="button">-</button>
                                        </div>
                                        <input type="number" v-model="form.sort" class="count form-control" min="0"
                                            step="1">
                                        <div class="input-group-append">
                                            <button class="btn text-monospace " v-on:click="addSort()"
                                                type="button">+</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="saveBtn-bg">
                                    <button class="saveBtn" type="button" v-on:click="update()">儲存</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="md-overlay"></div>
                {{-- modal end --}}
            </div>
        </div>
    </div>





    <script type="module">
        import Vue from '/frontend/js/vue.esm.browser.js';

        new Vue({
            el: "#news_page",
            data: {
                show: false,
                form: {

                },
                id: 0,
                newsType: [],
                selected: [],
            },
            components: {
                Multiselect: window.VueMultiselect.default
            },
            created: function() {

            },
            methods: {
                openModal(id) {
                    this.id = id;
                    axios.get('/api/news/' + id).then(x => {
                        this.form = x.data;
                        this.selected = x.data.news_type
                    })
                    this.show = true;
                    this.getNewsType();
                },
                getNewsType() {
                    axios.get('/api/news-type').then(x => {
                        this.newsType = x.data;
                    })
                },
                update() {
                    this.form.news_type_ids = this.selected.map(x => x.id);
                    this.form.publiced_at = this.form.date;
                    axios.put('/api/news/' + this.id, this.form).then(x => {
                        this.show = false;
                        this.form = {};
                        this.selected = [];
                        Swal.fire({
                            title: '修改成功',
                            icon: 'success',
                            showConfirmButton: false,
                            width: '300px',
                            timer: 1800,
                            heightAuto: false
                        })
                        location.reload();
                    }).catch((error) => {
                        errorMessage(error);
                    });
                }
            },
        });
    </script>

@endsection
