<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>佈告欄</title>
    <!-- <link rel="shortcut icon" href="images/shortcut_icon.png" type="image/x-icon"> -->
    <script src="https://kit.fontawesome.com/d17564c281.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://vjs.zencdn.net/5-unsafe/video-js.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <!-- <link rel='stylesheet' href='css/bootstrap.css'> -->
    <link rel='stylesheet' href='/frontend/css/main.css'>
</head>

<body>
    <div class="login">
        <div class="wrapper">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="input-group">
                    <input class="form-input" id="account" type="text" name="account"
                        placeholder="Acccount" required>
                    <span><i class="fas fa-user"></i></span>
                </div>
                <div class="input-group">
                    <input class="form-input" type="password" name="password" placeholder="Password" required>
                    <span><i class="fa fa-lock"></i></span>
                </div>
                <button class="submit">Login</button>
            </form>
        </div>
    </div>


    
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://vjs.zencdn.net/5-unsafe/video.js"></script>
<script src="/frontend/js/flatpickr.min.js"></script>
<script src="/frontend/js/rxjs.umd.js"></script>
<script type="text/javascript" src='/frontend/js/main.js'></script>

<script>
    // let element = document.getElementById('account')
    // rxjs.fromEvent(element, 'blur')
    //     .subscribe(res => {
    //         console.log('ac')
    //     });
</script>


</html>
