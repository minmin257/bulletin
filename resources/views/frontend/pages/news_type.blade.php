@extends('frontend.index')


@section('content')
    @include('frontend.components.test')
    <div class="app" id="app">
        @include('frontend.components.sidebar')
        <div class="wrapper">
            @include('frontend.components.navbar')
            <div class="main-container">
                <div class="main-body" id="news_type_page">
                    <div class="main-header anim" style="animation-delay: 0s;">分類管理
                        <div class="button-group md-trigger" data-modal="modal-1">
                            <button class="add" v-on:click="openModal()"><img
                                    src="https://img.icons8.com/ios-filled/18/ffffff/plus-math.png"></button>
                        </div>
                    </div>
                    <div class="members-content">
                        <div class="members-card anim" v-for="(item) in newsTypes">
                            <div
                                style="display: flex; justify-content: center; align-items: center; flex-direction: column;width: 100%;">
                                <div class="members-card-name">@{{ item . display_name }}</div>
                                <button class="members-card-edit" v-on:click="openModal(item.id)">
                                    <div class="members-card-dot "></div>
                                </button>

                                <button class="members-card-del" v-on:click="deleteNewsType(item.id)">
                                    <img src="https://img.icons8.com/fluent-systems-regular/18/353340/trash--v2.png" />
                                </button>
                            </div>

                        </div>


                    </div>



                    {{-- modal --}}

                    <div class="md-modal md-effect-1" v-bind:class="{ 'md-show': show }">
                        <div class="md-content">
                            <h5>
                                <span v-if="isUpdate">修改分類 </span>
                                <span v-if="!isUpdate">新增分類 </span>
                                <button class="md-close" v-on:click="show = false"><img
                                        src="https://img.icons8.com/ios-filled/14/353340/delete-sign--v2.png" /></button>
                            </h5>
                            <div class="md-content-body">
                                <div class="member-input">
                                    <div class="member">
                                        <label for="">分類名稱 Sort Name</label>
                                        <input v-model="form.display_name" type="text">
                                    </div>
                                    <div class="saveBtn-bg">
                                        <button class="saveBtn create_user_btn" type="button" v-on:click="save()"
                                            style="animation-delay: 0.2s;">儲存</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="md-overlay"></div>
                    {{-- end modal --}}
                </div>
            </div>
        </div>
    </div>


    <script type="module">
        import Vue from '/frontend/js/vue.esm.browser.js';
        new Vue({
            el: "#news_type_page",
            data: {
                newsTypes: [],
                show: false,
                form: {
                    display_name: "",
                    sort: 0,
                    enable: true,
                },
                isUpdate: false,
                id: 0,
            },
            components: {

            },
            created: function() {
                this.getNewsType();
            },
            methods: {
                resetForm() {
                    this.form = {
                        display_name: "",
                        sort: 0,
                        enable: true,
                    }
                },
                save() {
                    if (this.isUpdate) {
                        axios.put('/api/news-type/' + this.id, this.form).then(x => {
                            this.getNewsType();
                            this.resetForm();
                            this.show = false;
                            Swal.fire({
                                title: '修改成功',
                                icon: 'success',
                                showConfirmButton: false,
                                width: '300px',
                                timer: 1800,
                                heightAuto: false
                            })
                        }).catch((error) => {
                            errorMessage(error);
                        });

                        return;
                    }

                    axios.post('/api/news-type', this.form).then(x => {
                        this.getNewsType();
                        this.resetForm();
                        this.show = false;
                        Swal.fire({
                            title: '建立成功',
                            icon: 'success',
                            showConfirmButton: false,
                            width: '300px',
                            timer: 1800,
                            heightAuto: false
                        })
                    }).catch((error) => {
                        errorMessage(error);
                    });
                },
                openModal(id = null) {
                    this.resetForm();
                    this.show = true;

                    if (id) {
                        this.id = id;
                        axios.get('/api/news-type/' + id).then(x => {
                            this.form = x.data;
                            this.isUpdate = true;
                        })
                    } else this.isUpdate = false;
                },
                getNewsType() {
                    axios.get('/api/news-type').then(x => {
                        this.newsTypes = x.data;
                    })
                },
                deleteNewsType(id) {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger',
                            title: 'delete-swal-title',
                        },
                        buttonsStyling: false,
                        width: '300px'
                    })

                    swalWithBootstrapButtons.fire({
                        title: '您確定要刪除嗎？',
                        html: '<span style="color: #707070;font-size: 14px;">此動作將不可回復<span>',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '確定',
                        cancelButtonText: '取消',
                        reverseButtons: true,
                    }).then((result) => {
                        if (result.value) {
                            axios.delete('/api/news-type/' + id).then(x => {
                                Swal.fire({
                                    title: '刪除成功',
                                    icon: 'success',
                                    showCancelButton: false,
                                    showConfirmButton: false,
                                    timer: 1800,
                                    width: '300px',
                                    heightAuto: false
                                })
                                this.getNewsType();
                            }).catch((error) => {
                                errorMessage(error);
                            });

                        }
                    })
                }
            },
        });
    </script>


@endsection
