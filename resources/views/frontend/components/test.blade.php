 <!--新增會員-->
 <div class="md-modal md-effect-1" id="modal-1">
     <div class="md-content">
         <h5>新增分類
             <button class="md-close"><img
                     src="https://img.icons8.com/ios-filled/14/353340/delete-sign--v2.png" /></button>
         </h5>
         <div class="md-content-body">
             <div class="member-input">
                 <div class="member">
                     <label for="">分類名稱 Sort Name</label>
                     <input type="text" value="">
                 </div>
                 <div class="saveBtn-bg">
                     <button class="saveBtn create_user_btn" type="button" onclick="open_modal2()"
                         style="animation-delay: 0.2s;">儲存</button>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!--新增群組-->
 <div class="md-modal md-effect-1" id="modal-2">
     <div class="md-content">
         <h5>新增群組
             <button class="md-close"><img
                     src="https://img.icons8.com/ios-filled/14/353340/delete-sign--v2.png" /></button>
         </h5>
         <div class="md-content-body">
             <div class="member-input">
                 <div class="member">
                     <label for="">群組名稱 Name</label>
                     <input type="text" value="群組Ｓ">
                 </div>
                 <div class="saveBtn-bg">
                     <button class="saveBtn create_user_btn" type="button" onclick="open_modal2()"
                         style="animation-delay: 0.2s;">儲存</button>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!--新增權限類別-->
 <div class="md-modal md-effect-1" id="modal-3">
     <div class="md-content">
         <h5>新增權限類別
             <button class="md-close"><img
                     src="https://img.icons8.com/ios-filled/14/353340/delete-sign--v2.png" /></button>
         </h5>
         <div class="md-content-body">
             <div class="member-input">
                 <div class="member">
                     <label for="">權限類別名稱 Name</label>
                     <input type="text" value="系統管理者">
                 </div>
                 <div class="saveBtn-bg">
                     <button class="saveBtn" type="button" onclick="open_modal2()"
                         style="animation-delay: 0.2s;">儲存</button>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!--修改會員資料-->
 <div class="md-modal md-effect-1" id="modal-4">
     <div class="md-content">
         <h5>修改類別名稱
             <button class="md-close"><img
                     src="https://img.icons8.com/ios-filled/14/353340/delete-sign--v2.png" /></button>
         </h5>
         <div class="md-content-body">
             <div class="member-input">
                 <div class="member">
                     <label for="">權限類別名稱 Name</label>
                     <input type="text" value="類別">
                 </div>
                 <div class="saveBtn-bg">
                     <button class="saveBtn" type="button" onclick="open_modal2()"
                         style="animation-delay: 0.2s;">儲存</button>
                 </div>
             </div>
         </div>
     </div>
 </div>
