<!--新增消息-->
<div class="md-modal md-effect-1" id="modal-5" v-bind:class="{ 'md-show': show }">
    <div class="md-content">
        <h5>新增消息
            <button class="md-close" v-on:click="show = false"><img
                    src="https://img.icons8.com/ios-filled/14/353340/delete-sign--v2.png" /></button>
        </h5>
        <div class="md-content-body">
            <div class="member-input">
                <div class="member">
                    <label for="">內容 Content</label>
                    <textarea v-model="form.content" name="" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="member">
                    <label for="">消息分類 Sort</label>
                    <multiselect v-model="selected" placeholder="分類" label="display_name" track-by="id"
                        :options="newsType" :multiple="true" :taggable="true"></multiselect>
                </div>
                <div class="member">
                    <label for="">日期 Date</label>
                    <input class="flatpickr flatpickr-2 flatpickr-input active" type="date" v-model="form.date"
                        placeholder="選擇日期" readonly="readonly" style="opacity: 1; height: auto;">
                </div>
                <div class="member input-group spinner">
                    <label for="">排序 Sort</label>
                    <div style="display: flex;align-items: center">
                        <div class="input-group-prepend">
                            <button class="btn text-monospace " v-on:click="lessSort()" type="button">-</button>
                        </div>
                        <input type="number" v-model="form.sort" class="count form-control" min="0" step="1">
                        <div class="input-group-append">
                            <button class="btn text-monospace " v-on:click="addSort()" type="button">+</button>
                        </div>
                    </div>
                </div>
                <div class="saveBtn-bg">
                    <button class="saveBtn" type="button" v-on:click="save()">儲存</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="module">
    import Vue from './frontend/js/vue.esm.browser.js';

    new Vue({
        el: "#modal-5",
        data: {
            show: false,
            form: {
                sort: 1,
                enable: true
            },
            newsType: [],
            selected: [],
        },
        components: {
            Multiselect: window.VueMultiselect.default
        },
        created: function() {
            modalSubject.subscribe((v) => {
                if (v.target != 'news') return;
                this.show = true;
                this.getNewsType();
            });

        },
        methods: {
            getNewsType() {
                axios.get('/api/news-type').then(x => {
                    this.newsType = x.data;
                })
            },
            save() {

                this.form.news_type_ids = this.selected.map(x => x.id);
                this.form.publiced_at = this.form.date;
                axios.post('/api/news', this.form).then(x => {
                    this.form = {
                        sort: 1,
                        enable: true
                    };
                    this.selected = [];
                    Swal.fire({
                        title: '儲存成功',
                        icon: 'success',
                        showConfirmButton: false,
                        width: '300px',
                        timer: 1800,
                        heightAuto: false
                    })
                    this.show = false;
                    newOldSearch();
                }).catch((error) => {
                    errorMessage(error);
                });


            },
            addSort() {
                this.form.sort += 1;
            },
            lessSort() {
                if (this.form.sort == 1) return;
                this.form.sort -= 1;
            }
        },
    });
</script>
