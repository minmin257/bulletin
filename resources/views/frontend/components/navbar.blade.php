<div class="header">
    <div class="search-bar">
        <input type="text" placeholder="Search" id="news_search">
        <button id="news_search_btn">
            <img src="data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 56.966 56.966' fill='%23717790c7'%3e%3cpath d='M55.146 51.887L41.588 37.786A22.926 22.926 0 0046.984 23c0-12.682-10.318-23-23-23s-23 10.318-23 23 10.318 23 23 23c4.761 0 9.298-1.436 13.177-4.162l13.661 14.208c.571.593 1.339.92 2.162.92.779 0 1.518-.297 2.079-.837a3.004 3.004 0 00.083-4.242zM23.984 6c9.374 0 17 7.626 17 17s-7.626 17-17 17-17-7.626-17-17 7.626-17 17-17z'/%3e%3c/svg%3e"
                alt="">
        </button>
    </div>
    <div class="flatpickr-input-body">
        <div class="flatpickr-icon">
            <img src="https://img.icons8.com/material/20/353340/calendar.png" />
        </div>
        <input class="flatpickr flatpickr-input active" type="date" id="selectDate" placeholder="選擇日期。"
            readonly="readonly">
    </div>
    <div class="user-settings">
        <img class="user-img" src="{{ auth()->user()->profile_photo_url }}" alt="">
        <div class="user-name">
            {{ auth()->user()->name }}
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/frontend/js/rxjs.umd.js"></script>

<script>
    $(document).ready(function() {
        let date = [];
        let keyWord = "";
        let element = document.getElementById('news_search')
        rxjs.fromEvent(element, 'keyup')
            .subscribe(res => {
                keyWord = res.target.value
            });


        rxjs.fromEvent(document.getElementById('selectDate'), 'input')
            .subscribe(res => {
                let value = res.target.value;
                let check = value.includes("~")
                if (check) {
                    let result = value.split("~").map(x => x.trim());
                    date = result;
                    sendSearch();
                }                
            })

        rxjs.fromEvent(document.getElementById('news_search_btn'), 'click')
            .subscribe(res => {
                sendSearch();
            })


        function sendSearch() {
            sendSearchNew({
                date: date,
                keyWord: keyWord
            })
        }

    });

</script>
