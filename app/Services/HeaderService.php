<?php

    namespace App\Services;

    use App\Repositories\FrontPageRepository;

    class HeaderService
    {
        protected $frontPageRepository;

        public function __construct(FrontPageRepository $FrontPageRepository)
        {
            $this->frontPageRepository = $FrontPageRepository;
        }

        public function getAllTreeForFront()
        {
            return  $this->frontPageRepository->getAllTree();
        }
    }
