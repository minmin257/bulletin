<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\NewsType;
use App\Repositories\Base\Repository;
use Carbon\Carbon;

class NewsRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return News::class;
    }

    /**
     * 分頁(某個NewType下的分頁)
     */
    public function pageByNewTypeId($typeId, $count = 10)
    {
        return $this->getNewsByTypeIdQuery($typeId)->paginate($count);
    }


    /**
     * 建立News 及關聯
     */
    public function createWithType($input,  $type)
    {
        $item = new News($input);
        $item->save();
        $item->news_type()->attach($type);
    }


    /**
     * 前台頁面讀取news，預設為取得日期最近五筆news
     */
    public function getCloseNewsForFront($typeId, $take = 5)
    {
        $now = Carbon::now()->format('Y-m-d');
        return $this->getNewsByTypeIdQuery($typeId)
            ->where('enable', true)
            ->where('publiced_at', '<=', $now)
            ->where(function ($query) use ($now) {
                $query->whereNull('closed_at')->orWhere('closed_at', '>=', $now);
            })
            ->take($take)->get();
    }

    /**
     * where typeId
     * orderby date、sort
     * news 實體查詢
     * @return query
     */
    private function getNewsByTypeIdQuery($typeId)
    {
        return $this->getQuery()->whereHas('news_type', function ($query) use ($typeId) {
            $query->where('id', $typeId);
        })->orderBy('date','desc')->orderBy('sort');
    }
}
