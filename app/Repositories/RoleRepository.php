<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Repositories\Base\Repository;

class RoleRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Role::class;
    }


    /**
     * 取得多個角色
     * @param  array $ids
     * @return array
     */
    public function getRolesByIds($ids)
    {
        return $this->model()::whereIn('id', $ids)->get();
    }

    public function getRolesById($id)
    {
        return $this->model()::where('id', $id)->first();
    }
    
}
