<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\NewsType;
use App\Repositories\Base\Repository;

class NewsTypeRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return NewsType::class;
    }


    /**
     *   取得有啟用的newTypes
     *  @return  array 
     */
    public function getAvalibleTypes()
    {
        return $this->model::where('enable', true)->orderBy('sort')->get();
    }


    /**
     *   取得有啟用的newType,如果沒有將取得第一筆
     *   @return NewsType
     */
    public function getAvalibleTypeById($id = 0)
    {
        $query = $this->getQuery()->where('enable', true);
        if ($id == 0) {
            return $query->orderBy('sort')->first();
        }
        return $query->where('id', $id)->orderBy('sort')->firstOrFail();
    }


    public function getByIds($ids)
    {
        return $this->model::whereIn('id', $ids)->get();
    }
}
