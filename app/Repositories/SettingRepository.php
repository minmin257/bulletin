<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Repositories\Base\Repository;

class SettingRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Setting::class;
    }


    /**
     * 以key值取得value
     * @return Setting
     */
    public function getByKey(string $key)
    {
        return $this->model()::where('key', $key)->first();
    }

    /**
     *   以key值的第一個參數做為搜尋條件
     *   such as：
     *   $word = video
     *   output: video.youtube.url、video.facebook.url,
     *  @return array
     */
    public function getByFirstWord(string $word)
    {
        return $this->model()::where('key', 'like', $word . '.%')->get();
    }


    /**
     *   修改某個key的value    
     *   @return Setting     
     */
    public function setByKey(string $key, string $value)
    {
        return $this->model->where('key', $key)->update(['value' => $value]);
    }
}
