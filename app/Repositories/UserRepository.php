<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\Base\Repository;

class UserRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return User::class;
    }


}
