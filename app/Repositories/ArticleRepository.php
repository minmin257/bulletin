<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Repositories\Base\Repository;

class ArticleRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Article::class;
    }
}