<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Subscription;

class SubscriptionExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $export = new Collection();
        $subscriptions = Subscription::all();
        foreach ($subscriptions as $subscription) {
            $export->push([
                'Email'        => $subscription->email,
                'First Name'   => $subscription->first_name,
                'Last Name'    => $subscription->last_name,
                'Country'      => $subscription->country,
                'Join_meeting' => ($subscription->join_meeting)?'Yes':'no',
                'Subscript At' => $subscription->created_at,
            ]);
        }

        return $export;
    }


    public function headings(): array
    {
        $headings = [
            'Email',
            'First Name',
            'Last Name',
            'Country',
            'Join Meeting',
            'Subscript At',
        ];

        return $headings;
    }
}
