<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;
class LoginResponse implements LoginResponseContract
{
    /**
     * @todo 登入後導頁問題：是否為admin才導入後台？
     * @param  $request
     * @return mixed
     */
    public function toResponse($request)
    {
    //     $is_admin = collect(auth()->user()->roles)->map(function($x){
    //         return $x->name;
    //     })->contains('admin');
        
    //     $home = $is_admin ? '/dashboard' : '/';

        return redirect()->intended('/');
    }
}
