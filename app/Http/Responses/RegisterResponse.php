<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\RegisterResponse as RegisterResponseContract;
class RegisterResponse implements RegisterResponseContract
{
    /**
     * @todo 註冊後導頁問題：是否為admin才導入後台？
     * @param  $request
     * @return mixed
     */
    public function toResponse($request)
    {
        $home = auth()->user()->is_admin ? '/admin' : '/dashboard';

        return redirect()->intended($home);
    }
}
