<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        $user = $request->user();
        if (!empty($user)) {
            $rolePermission = collect($user->roles)->map(function ($roles) {
                return $roles->permissions; //json_decode($roles->permissions);
            });
            $permission = collect($rolePermission)->flatten()->unique()->values();
        } else $permission = [];



        return array_merge(parent::share($request), [
            "canGoogleLogin" => false,
            "canFacebookLogin" => false,
            "canLineLogin" => false,
            "permission" => $permission
        ]);
    }
}
