<?php

namespace App\Http\Controllers\API\Example;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ArticleRepository;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ExampleSheet;
use \PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Support\Str;

// 測試用api
class ExampleController extends Controller
{
    public function readExcel()
    {
        $sheetName = "工作表2";
        $fileName = "terry_file.xlsx";
        
        $records = Excel::toCollection(new ExampleSheet(), public_path($fileName));

        $collection = $records[$sheetName]
            ->filter(function ($value, $key) {
                return trim($value['0']) != '';
            })
            ->map(function ($item) {
                $date = Date::excelToDateTimeObject($item[0])->format('Y-m-d');
                return [
                    "時間" => $date,
                    "分類"   => $item[1],
                    "內容"  => $item[2]
                ];
            });

        $strType = $collection->map(function ($item) {
            return trim($item['分類']);
        })->join('、');
        $typeArray = Str::of($strType)->split('/、/');
        $newsTypeLabel = collect($typeArray)->unique()->values()->all();
        // dump($newsTypeLabel);

        // create news type


        return $collection->all();
    }


    public function quicklyTestApi(Request $request)
    {
        return response('Hello World', 200);
    }
}
