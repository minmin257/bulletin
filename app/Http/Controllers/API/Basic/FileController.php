<?php

namespace App\Http\Controllers\API\Basic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class FileController extends Controller
{
    public function __construct()
    {
    }


    public function uploadImage(Request $request)
    {
        $request->validate([
            'image' => ['required', 'mimes:jpg,jpeg,png', 'max:2048'],
            'folder' => [
                Rule::in(['tinymce', 'invitee', 'album','frontPage']),
                'required'
            ]
        ]);

        $path = Storage::disk('public')->putFile('image/' . $request->folder, $request->file('image'));

        return response()->json([
            'file_path' => "/storage/" . $path
        ]);
    }
}
