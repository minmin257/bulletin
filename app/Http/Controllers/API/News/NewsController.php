<?php

namespace App\Http\Controllers\API\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NewsRepository;
use App\Repositories\NewsTypeRepository;
use Gate;
class NewsController extends Controller
{

    private $newsRepository, $newsTypeRepository;

    public function __construct(
        NewsRepository $NewsRepository,
        NewsTypeRepository $NewsTypeRepository
    ) {
        $this->newsRepository = $NewsRepository;
        $this->newsTypeRepository = $NewsTypeRepository;
    }




    public function index(Request $request)
    {
        $keyword = $request->query('keyword', '');
        $start_date = $request->query('startDay', null);
        $end_date = $request->query('endDay', null);
        $query = $this->newsRepository->getQuery();

        if (isset($start_date) && isset($end_date)) $query->whereBetween('date', [$start_date, $end_date]);
        if (isset($keyword)) {
            $query->where(function ($sub_query) use ($keyword) {
                $sub_query->where('title', 'like', '%' . $keyword . '%')
                    ->orWhere('content', 'like', '%' . $keyword . '%')
                    ->orWhereHas('news_type', function ($query) use ($keyword) {
                        $query->where('display_name','like', '%'.$keyword.'%');
                    });
            });
        }

        if (Gate::denies('News.Management')) {
            $query->where('enable', true);
        }

        return $query->orderBy('date','desc')->orderBy('sort','desc')->paginate(100);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // 'title' => ['string', 'required'],
            'content' => ['string', 'required'],
            'sort' => ['numeric'],
            'enable' => ['boolean'],
            'date' => ['date', 'required'],
            'publiced_at' => ['date', 'nullable'],
            'closed_at' => ['date', 'nullable'],
            'news_type_ids' => ['array']
        ]);

        $request->merge([
            'creator_user_id' => $request->user()->id
        ]);

        $fillable = $this->newsRepository->getFillable();
        $newTypes =  $this->newsTypeRepository->getByIds($request->news_type_ids);
        $this->newsRepository->createWithType(
            $request->only($fillable),
            $newTypes
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->newsRepository->getById($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            // 'title' => ['string', 'required'],
            'content' => ['string', 'required'],
            'sort' => ['numeric'],
            'enable' => ['boolean'],
            'date' => ['date'],
            'publiced_at' => ['date','nullable'],
            'closed_at' => ['date', 'nullable'],
            'news_type_ids' => ['array']
        ]);

        $fillable = $this->newsRepository->getFillable();
        $news = $this->newsRepository->getById($id);
        $news->news_type()->sync($request->input('news_type_ids', []));
        $this->newsRepository->update(
            $request->only($fillable),
            $id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ids = $request->query('ids', []);
        $this->newsRepository->getQuery()->whereIn('id', $ids)->delete();
    }


    public function changeEnable(Request $request)
    {
        $ids = $request->query('ids', []);
        $news = $this->newsRepository->getQuery()->whereIn('id', $ids)->get();

        foreach ($news as $n) {
            $n->update(['enable'=>!$n->enable]);
        }
    }
}
