<?php

namespace App\Http\Controllers\API\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NewsTypeRepository;

class NewsTypeController extends Controller
{

    private $newsTypeRepository;

    public function __construct(
        NewsTypeRepository $NewsTypeRepository
    ) {
        $this->newsTypeRepository = $NewsTypeRepository;
    }


    public function index()
    {
        return $this->newsTypeRepository->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'display_name' => ['string', 'required'],
            'sort' => ['numeric'],
            'enable' => ['boolean'],
        ]);

        $fillable = $this->newsTypeRepository->getFillable();
        $this->newsTypeRepository->create(
            $request->only($fillable)
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->newsTypeRepository->getById($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'display_name' => ['string', 'required'],
            'sort' => ['numeric'],
            'enable' => ['boolean'],
        ]);

        $fillable = $this->newsTypeRepository->getFillable();
        $this->newsTypeRepository->update(
            $request->only($fillable),
            $id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->newsTypeRepository->destroy($id);
    }
}
