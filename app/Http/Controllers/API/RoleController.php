<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\RoleRepository;
use Gate;
use App\Traits\PermissionTrait;

class RoleController extends Controller
{
    use PermissionTrait;
    private $roleRepository;

    public function __construct(RoleRepository $RoleRepository)
    {
        $this->roleRepository = $RoleRepository;
    }

    /**
     * 取得所有角色
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return $this->roleRepository->getAll();
    }



    /**
     * 取得角色
     *
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        return $this->roleRepository->getById($id);
    }

    /**
     * 建立角色
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        $request->validate([
            'name' => ['string', 'required'],
            'description' => ['string', 'nullable'],           
        ]);

        if (Gate::denies('Role.Create')) {
            return response()->json([
                'message' => '你沒有權限'
            ], 403);
        }
        $this->roleRepository->create($request->only([
            "name", "description", "is_default", "permissions"
        ]));
    }

    /**
     * 修改角色
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('Role.Update')) {
            return response()->json([
                'message' => '你沒有權限'
            ], 403);
        }

        $this->roleRepository->update($request->only([
            "name", "description", "is_default", "permissions"
        ]), $id);
    }

    /**
     * 刪除角色
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (Gate::denies('Role.Delete')) {
            return response()->json([
                'message' => '你沒有權限'
            ], 403);
        }

        $this->roleRepository->destroy($id);
    }

    /**
     * 取得所有permission
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllPermission(Request $request)
    {
        $collection = $this->getPermissionMap();
        return response()->json([
            'permissions' => $collection
        ]);;
    }
}
