<?php

namespace App\Http\Controllers\API\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SettingRepository;
use App\Traits\Settings\TinymceSettingTrait;
use App\Traits\Settings\ContentSettingTrait;

class SettingController extends Controller
{

    use TinymceSettingTrait, ContentSettingTrait;
    private $settingRepository;

    public function __construct(SettingRepository $SettingRepository)
    {
        $this->settingRepository = $SettingRepository;
    }

    /**
     * 取得tinymce api key
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTinymceApiKey(Request $request)
    {
        return $this->getTinymceKey();
    }

    /**
     * 設定tinymce api key
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setTinymceApiKey(Request $request)
    {
        return $this->setTinymceKey($request->api_key);
    }

    /**
     * 取得youtbue url
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getVideoYoutbeUrlSetting(Request $request)
    {
        return $this->getVideoYoutbeUrl();
    }


    /**
     * 設定youtbue url
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setVideoYoutbeUrlSetting(Request $request)
    {
        return $this->setVideoYoutbeUrl($request->url);
    }

    /**
     * 取得Important Datas Text
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getImportantDatasTextSetting(Request $request)
    {
        return $this->getImportantDatasText();
    }

    /**
     * 設定Important Datas Text
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setImportantDatasTextSetting(Request $request)
    {
        return $this->setImportantDatasText($request->text);
    }

    /**
     * 取得 Quick Connect Group
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getQuickConnectGroupSetting()
    {
        return $this->getQuickConnectGroup();
    }

    /**
     * 設定 Quick Connect Group
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setQuickConnectGroupSetting(Request $request)
    {
        return $this->setQuickConnectGroup($request->group);
    }
}
