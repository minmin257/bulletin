<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Gate;
use App\Actions\Fortify\PasswordValidationRules;

class UserController extends Controller
{
    use PasswordValidationRules;
    private $userRepository, $roleRepository;

    public function __construct(
        UserRepository $UserRepository,
        RoleRepository $RoleRepository
    ) {
        $this->userRepository = $UserRepository;
        $this->roleRepository = $RoleRepository;
    }

    /**
     * 取得使用者
     *
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        return $this->userRepository->getById($id);
    }



    /**
     * 建立使用者
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('User.Create')) {
            return response()->json([
                'message' => '你沒有權限'
            ], 403);
        }

        $request->validate([
            'account' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'role_ids' => ['required']
        ]);

        $user = $this->userRepository->create([
            'account' => $request->account,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $roles = $this->roleRepository->getRolesByIds($request->input('role_ids', []));
        $user->roles()->attach($roles);
    }



    /**
     * 修改使用者
     * @todo 可以修改密碼或另開api
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('User.Update')) {
            return response()->json([
                'message' => '你沒有權限'
            ], 403);
        }

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            // 'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'password' => $this->passwordRules(),
            'role_ids' => ['required']
        ]);
        $user = $this->userRepository->getById($id);
        $user->roles()->sync($request->input('role_ids', []));
        $this->userRepository->update([
            'name' => $request->name,
            // 'email' => $request->email
        ], $id);
    }

    //wan excrice
    public function getWithRole($id)
    {
        return $this->roleRepository->getRolesById($id)->users;
    }
}
