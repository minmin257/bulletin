<?php

namespace App\Http\Controllers\PermissionManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Repositories\SettingRepository;
use App\Repositories\RoleRepository;
use Gate;

class RoleController extends Controller
{
    private $roleRepository;

    public function __construct(RoleRepository $RoleRepository)
    {
        $this->roleRepository = $RoleRepository;
    }

    public function roleSetting(Request $request)
    {

        if (Gate::denies('Page.PermissionManagement.RoleSetting')) {
            abort(403,"你沒有權限");
        }

        $pageData = $this->roleRepository->page();
        return Inertia::render('PermissionManagement/RoleSetting', [
            "pageData" => $pageData
        ]);
    }
}
