<?php

namespace App\Http\Controllers\PermissionManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Repositories\UserRepository;
use Gate;


class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $UserRepository)
    {
        $this->userRepository = $UserRepository;
    }

    public function userSetting(Request $request)
    {

        
        if (Gate::denies('Page.PermissionManagement.UserSetting')) {
            abort(403,"你沒有權限");
        }

        $pageData = $this->userRepository->page();
        return Inertia::render('PermissionManagement/UserSetting', [
            "pageData" => $pageData
        ]);
    }
}
