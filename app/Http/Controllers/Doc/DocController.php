<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Repositories\SettingRepository;
use App\Repositories\RoleRepository;
use Gate;



class DocController extends Controller
{
   
    private $settingRepository,$RoleRepository;

    public function __construct(
        SettingRepository $SettingRepository,
        RoleRepository $RoleRepository
        )
    {
        $this->settingRepository = $SettingRepository;
        $this->RoleRepository = $RoleRepository;
    }

    public function example(Request $request){
        if (Gate::denies('Page.Doc')) {
            abort(403,"你沒有權限");
        }

        return Inertia::render('Doc/Example', ["backendMsg" => '此字串來自後端的資料！']);
    }

    public function index(Request $request){

        if (Gate::denies('Page.Doc')) {
            abort(403,"你沒有權限");
        }

        return Inertia::render('Doc/Index');
    }

    public function blank(Request $request){

        if (Gate::denies('Page.Doc')) {
            abort(403,"你沒有權限");
        }

        return Inertia::render('Doc/Blank');
    }

    public function test1(Request $request){

        if (Gate::denies('Page.Doc')) {
            abort(403,"你沒有權限");
        }
        
        return Inertia::render('Doc/Test1');
    }

    public function test2(Request $request){
        if (Gate::denies('Page.Doc')) {
            abort(403,"你沒有權限");
        }
        
        $apiKey = $this->settingRepository->getByKey('tinymce.api.key');
        return Inertia::render('Doc/Test2',[
            "tinymceApiKey" => $apiKey->value
        ]);
    }


    public function test3(Request $request){
        $Roles = $this->RoleRepository->getAll();
        return Inertia::render('Doc/Test3',[
            "roles" => $Roles
        ]);
    }


}
