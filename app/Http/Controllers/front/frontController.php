<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SettingRepository;
use App\Repositories\NewsTypeRepository;
use App\Repositories\NewsRepository;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Gate;

class frontController extends Controller
{
    private $newsRepository;

    public function __construct(
        NewsRepository $NewsRepository
    ) {
        $this->newsRepository = $NewsRepository;
    }



    public function login()
    {
        return view(
            'frontend.pages.login'
        );
    }

    public function home()
    {
        $newsPermission = true;
        if (Gate::denies('News.Management')) {
            $newsPermission = false;
        }
        return view(
            'frontend.pages.home',
            compact('newsPermission')
        );
    }

    public function newsType()
    {
        if (Gate::denies('Page.NewsManagement.NewsTypeSetting')) {
            abort(403, "你沒有權限");
        }

        return view(
            'frontend.pages.news_type'
        );
    }

    public function editNews(Request $request, $id)
    {
        if (Gate::denies('News.Management')) {
            abort(403,"你沒有權限");
        }
        $news = $this->newsRepository->getById($id);
        $canEdit = $news->creator_user_id == $request->user()->id;
        return view(
            'frontend.pages.edit_news',
            compact('news', 'canEdit')
        );
    }
}
