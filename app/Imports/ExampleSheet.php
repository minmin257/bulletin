<?php



namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Imports\ExampleImport;

class ExampleSheet implements WithMultipleSheets 
{
   
    public function sheets(): array
    {
        return [
            '工作表1'  => new ExampleImport(),
            '工作表2'  => new ExampleImport(),
        ];
    }
}