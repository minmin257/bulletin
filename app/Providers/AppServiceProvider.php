<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\ServiceProvider;
use App\Traits\PermissionTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class AppServiceProvider extends ServiceProvider
{
    use PermissionTrait;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $collection = $this->getPermissionMap();
        foreach ($collection as $p) {
            $gate->define($p['key'], function ($user) use ($p) {
                $rolePermission = collect($user->roles)->map(function ($roles) {
                    return $roles->permissions;//json_decode($roles->permissions);
                });
                $permission = collect($rolePermission)->flatten()->unique();
                return $permission->contains($p['key']);
            });
        }

        JsonResource::withoutWrapping();
    }
}
