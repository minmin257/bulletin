<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\NewsType;
class News extends Model
{
    use HasFactory;

    protected $with = ['news_type'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'date',
        'publiced_at',
        'closed_at',
        'sort',
        'enable',
        'creator_user_id'
    ];


     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'enable' => 'boolean'
    ];



    // relation
    public function news_type()
    {
        return $this->belongsToMany(NewsType::class);
    }

}
