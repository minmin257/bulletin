<?php

namespace App\Traits;

use Exception;
use Illuminate\Http\Request;

trait PermissionTrait
{

    public function getPermissionMap()
    {
        $permission_map = require config_path('permission-map.php');
        $collection = collect($permission_map)->all();
        return $collection;
    }
}
