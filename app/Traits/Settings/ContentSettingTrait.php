<?php

namespace App\Traits\Settings;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait ContentSettingTrait
{
    /**
     * @var App\Repositories\SettingRepository;
     */
    private $settingRepository;

    //setting Key
    private $videoYoutbeUrl = "video.youtube.url";

    private $quickConnectGroup = "quick_connect";
    private $quickConnectRegistrationUrl = "quick_connect.registration.url";
    private $quickConnectProgramUrl = "quick_connect.program.url";
    private $quickConnectAbstractSubmissionUrl =  "quick_connect.abstract_submission.url";
    private $quickConnectAccommodationUrl = "quick_connect.accommodation.url";

    private $importantDatasText = "important_datas.text";

    // VideoYoutbeUrl
    public function getVideoYoutbeUrl()
    {
        $result = $this->settingRepository->getByKey($this->videoYoutbeUrl);
        return $result;
    }

    public function setVideoYoutbeUrl($value)
    {
        $this->settingRepository->setByKey($this->videoYoutbeUrl, $value);
    }

    // ImportantDatasText
    public function getImportantDatasText()
    {
        $result = $this->settingRepository->getByKey($this->importantDatasText);
        return $result;
    }

    public function setImportantDatasText($value)
    {
        $this->settingRepository->setByKey($this->importantDatasText, $value);
    }


    // QuickConnectGroup
    public function getQuickConnectGroup()
    {
        $result = $this->settingRepository->getByFirstWord($this->quickConnectGroup);
        return $result;
    }


    // 設定group setting
    public function setQuickConnectGroup($arrays)
    {
        foreach ($arrays as $data) {
            if (!Str::startsWith($data['key'], $this->quickConnectGroup . '.')) continue;
            $this->settingRepository->setByKey($data['key'],$data['value']);
        }
    }
}
