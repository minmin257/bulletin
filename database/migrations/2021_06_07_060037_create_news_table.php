<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            // $table->integer('news_type_id')->unsigned();
            $table->integer('creator_user_id')->nullable();
            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->date('date');
            $table->date('publiced_at');
            $table->date('closed_at')->nullable();          
            $table->integer('sort')->default(0);
            $table->boolean('enable')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
