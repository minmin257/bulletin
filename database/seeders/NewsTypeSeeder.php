<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class NewsTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = file_get_contents("database/data/newTypes.json");
        $datas = json_decode($file);

        foreach ($datas as $data) {
            $check = DB::table('news_types')->where('display_name', $data->display_name)->first();

            if (empty($check)) {
                DB::table('news_types')->insert([
                    'sort' => $data->sort,
                    'display_name' => $data->display_name,
                    'enable' => $data->enable,
                ]);
            }
        }

    }
}
