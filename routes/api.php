<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//專案用Api
Route::resource('news-type', API\News\NewsTypeController::class);

Route::put('/news/change-enable',[API\News\NewsController::class,'changeEnable']);
Route::delete('/news/delete-many',[API\News\NewsController::class,'destroy']);
Route::resource('news', API\News\NewsController::class)->except(['destroy']);

//wan excrice
Route::get('wantest/user/{role}', [API\UserController::class, 'getWithRole_wantest']);


// 設定 Api
require __DIR__ . '/api/setting.php';

//使用者、角色 Api
require __DIR__ . '/api/basic.php';

// 共用Api
require __DIR__ . '/api/command.php';

// example api
require __DIR__ . '/api/example.php';