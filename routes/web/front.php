<?php

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\front;

//前台頁面
Route::get('/login',  [front\frontController::class, 'login'])
->middleware(['guest:'.config('fortify.guard')])
->name('login');
Route::name('front.')->group(function () {
    Route::middleware(['auth:sanctum', 'verified'])->group(function(){
        Route::get('/',  [front\frontController::class, 'home'])->name('home');
        Route::get('/news-type',[front\frontController::class, 'newsType'])->name('newsType');
        Route::get('/edit-news/{id}',  [front\frontController::class, 'editNews'])->name('editNews');
    });

});

