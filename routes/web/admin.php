<?php

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers;


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');





//---------------以下為新版-------------------

//後台頁面
Route::middleware(['auth:sanctum', 'verified'])->name('admin.')->prefix("admin")->group(function () {

    Route::get('/team-select', function () {
        return Inertia::render('Teams/TeamSelect');
    })->name('teams.teamSelect');


    Route::prefix("doc")->name('doc.')->group(function () {
        Route::get('/example',  [Controllers\Doc\DocController::class, 'example'])->name('example');
        Route::get('/index', [Controllers\Doc\DocController::class, 'index'])->name('index');
        Route::get('/blank',  [Controllers\Doc\DocController::class, 'blank'])->name('blank');
        //測試用頁面
        Route::get('/test1',  [Controllers\Doc\DocController::class, 'test1'])->name('test1');
        Route::get('/test2',  [Controllers\Doc\DocController::class, 'test2'])->name('test2');
        Route::get('/test3',  [Controllers\Doc\DocController::class, 'test3'])->name('test3');
    });



    Route::prefix("permission-management")->name('permission-management.')->group(function(){        
        Route::get('/role-setting',  [Controllers\PermissionManagement\RoleController::class, 'roleSetting'])->name('roleSetting');
        Route::get('/user-setting',  [Controllers\PermissionManagement\UserController::class, 'userSetting'])->name('userSetting');
    });


    Route::get('/', function () {
        return redirect('/dashboard');
    });
});
