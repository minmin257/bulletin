<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Example;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;



// Api example
Route::resource('article', Example\ArticleController::class);
Route::get('example/read-excel', [Example\ExampleController::class, 'readExcel']);
Route::get('example/quickly-test-api', [Example\ExampleController::class, 'quicklyTestApi']);