<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;




//使用者 Api
Route::prefix('user')->name('user.')->group(function () {
    // 取得使用者
    Route::get('{id}', [API\UserController::class, 'get'])->name('get');
    // 新增使用者
    Route::post('', [API\UserController::class, 'store'])->name('store');
    // 修改使用者
    Route::put('{id}', [API\UserController::class, 'update'])->name('update');;
});

//角色 Api
Route::prefix('role')->name('role.')->group(function () {
    // 取得所有權限資料
    Route::get('get-all-permission', [API\RoleController::class, 'getAllPermission']);
    // 取得所有角色
    Route::get('', [API\RoleController::class, 'getAll'])->name('getAll');
    // 取得角色
    Route::get('{id}', [API\RoleController::class, 'get'])->name('get');
    // 新增角色
    Route::post('', [API\RoleController::class, 'store'])->name('store');
    // 修改角色
    Route::put('{id}', [API\RoleController::class, 'update'])->name('update');
    // 刪除角色
    Route::delete('{id}', [API\RoleController::class, 'delete'])->name('delete');
});

Route::prefix('file')->name('file.')->group(function () {
    Route::post('upload-image',[API\Basic\FileController::class,'uploadImage'])->name('uploadImage');
});

