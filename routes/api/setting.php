<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Setting;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;





Route::prefix('setting')->name('setting.')->group(function(){
    Route::get('tinymce-api-key', [Setting\SettingController::class,'getTinymceApiKey']);
    Route::put('tinymce-api-key', [Setting\SettingController::class,'setTinymceApiKey']);

    Route::get('video-youtbe-url', [Setting\SettingController::class,'getVideoYoutbeUrlSetting']);
    Route::put('video-youtbe-url', [Setting\SettingController::class,'setVideoYoutbeUrlSetting']);

    Route::get('important-datas-text', [Setting\SettingController::class,'getImportantDatasTextSetting']);
    Route::put('important-datas-text', [Setting\SettingController::class,'setImportantDatasTextSetting']);

    Route::get('quick-connect-group', [Setting\SettingController::class,'getQuickConnectGroupSetting']);
    Route::put('quick-connect-group', [Setting\SettingController::class,'setQuickConnectGroupSetting']);

});



