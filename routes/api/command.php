<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;





//reset file Storage link api
Route::get('/execution/storage-link', function () {
    // $file_path = public_path('storage');
    // if (File::exists($file_path)) File::deleteDirectory($file_path);
    Artisan::call('storage:link');
    return response()->json([
        'result' => true
    ]);;
});


//clear cache
Route::get('/execution/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    return response()->json([
        'result' => true
    ]);;
});


//generate key
Route::get('/execution/generate-key', function () {
    Artisan::call('key:generate');
    return response()->json([
        'result' => true
    ]);;
});


//run seed
Route::get('/execution/db-seed', function () {
    Artisan::call('db:seed');
    return response()->json([
        'result' => true
    ]);;
});
