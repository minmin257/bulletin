<?php

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers;
use App\Http\Controllers\SubscriptionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// UI畫面
require __DIR__ . '/web/admin.php'; //後台頁面
require __DIR__ . '/web/front.php'; //前台頁面
require __DIR__ . '/web/file.php';  //檔案上傳、檔案匯出、檔案匯入

Route::get('/webmin', function () {
    return redirect('/admin');
});

//404 將導到前台頁面
Route::get('{any}', function () {
    return redirect('/');
})->where('any', '.*');
