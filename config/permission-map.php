<?php

return [
    [
        'label' => '角色新增',
        'key' => 'Role.Create'
    ],
    [
        'label' => '角色修改',
        'key' => 'Role.Update'
    ],
    [
        'label' => '角色刪除',
        'key' => 'Role.Delete'
    ],
    [
        'label' => '文檔系統',
        'key' => 'Page.Doc'
    ],
    [
        'label' => '使用者建立',
        'key' => 'User.Create'
    ],
    [
        'label' => '使用者修改',
        'key' => 'User.Update'
    ],
    [
        'label' => '最新消息管理',
        'key' => 'News.Management'
    ],



    // 頁面相關
    [
        'label' => '消息管理',
        'key' => 'Page.ContentManagement.NewsSetting'
    ],
    [
        'label' => '會員設定',
        'key' => 'Page.PermissionManagement.UserSetting'
    ],
    [
        'label' => '角色設定',
        'key' => 'Page.PermissionManagement.RoleSetting'
    ],
    [
        'label' => '最新消息種類管理',
        'key' => 'Page.NewsManagement.NewsTypeSetting'
    ]



];
